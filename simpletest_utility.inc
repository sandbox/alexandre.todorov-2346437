<?php
class SimpleTestUtilityHelper {

  public static function dumpPregReplace(SimpleTestUtilityTestCase $self) {
    global $databases;

    $public_files_directory  = $self->getOriginalFileDirectory() . '/simpletest/' . substr($self->getDatabasePrefix(), 10);
//     $private_files_directory = $public_files_directory . '/private';
//     $temp_files_directory    = $private_files_directory . '/temp';

//     echo
    $filename = $public_files_directory . '/mysqldump.sql';
    if(!file_exists($filename)) {
      //       var_export($databases);
      // array ( 'default' => array ( 'default' => array ( 'database' => 'unltd_new', 'username' => 'root', 'password' => 'PY52dkFM', 'host' => 'localhost', 'driver' => 'mysql', 'prefix' => '', ), ), 'legacy' => array ( 'default' => array ( 'database' => 'rse-demo', 'username' => 'root', 'password' => 'PY52dkFM', 'host' => 'localhost', 'port' => '', 'driver' => 'mysql', 'prefix' => '', ), ), )

      $output = array();
      $return_value = 0;

//     echo
      $cmd = 'cd ' . DRUPAL_ROOT . '; drush sql-dump --structure-tables-key=common --result-file=' . $filename;
//       $cmd = 'mysqldump'
//           . ' -h ' . $databases['default']['default']['host']
//           . ' -u ' . $databases['default']['default']['username']
//           . ' -p' . $databases['default']['default']['password']
//           . ' ' . $databases['default']['default']['database']
//           . ' > ' . $filename;
      exec($cmd, $output, $return_value);

      if($return_value !== 0) {
        throw new Exception(t('Error return status from !cmd', array('!cmd' => $cmd)));
      }
      //        var_export(array($output, $return_value));
      //        print_r(array($output, $return_value));

      //  dprint_r($filename);


      $handle = fopen($filename, "rb");
      $sql = stream_get_contents($handle);
      fclose($handle);

//       $cte = '_' . TESTS_APP_ENV;
      $sql = preg_replace('/(DROP\s+TABLE\s+IF\s+EXISTS\s+`)([^`]+)(`;)/', '\\1' . $self->getDatabasePrefix() . '\\2\\3', $sql);
      $sql = preg_replace('/(CREATE\s+TABLE\s+`)([^`]+)(`\s+\()/', '\\1' . $self->getDatabasePrefix() . '\\2\\3', $sql);
      $sql = preg_replace('/(LOCK\s+TABLES\s+`)([^`]+)(`\s+WRITE;)/', '\\1' . $self->getDatabasePrefix() . '\\2\\3', $sql);
      $sql = preg_replace('/(ALTER\s+TABLE\s+`)([^`]+)(`\s+DISABLE\s+KEYS)/', '\\1' . $self->getDatabasePrefix() . '\\2\\3', $sql);
      $sql = preg_replace('/(INSERT\s+INTO\s+`)([^`]+)(`\s+VALUES)/', '\\1' . $self->getDatabasePrefix() . '\\2\\3', $sql);
      $sql = preg_replace('/(ALTER\s+TABLE\s+`)([^`]+)(`\s+ENABLE\s+KEYS)/', '\\1' . $self->getDatabasePrefix() . '\\2\\3', $sql);
      //  INSERT INTO `actions` VA
      //  LOCK TABLES `actions` WRITE;
      //  $sql = preg_replace('/(INSERT\s+)(INTO\s+`[^`]+`)/', '\\1IGNORE \\2', $sql);

      //       $filename = realpath(TESTSDATAS_PATH) . '/' . SITEPRF . '_test_out.sql';

      //  dprint_r($filename);

      $fh = fopen($filename, 'w');

      if ($fh === FALSE) {
        throw new RuntimeException("Could not open {$filename} for writing see " . __CLASS__ . "::setFileName()");
      }

      fwrite($fh, $sql);
      fclose($fh); /**/

      //       $cmd = 'mysql -h ' . TESTS_MYSQL_HOST . ' -u ' . SITEPRF . ' --password=eiEs54pY ' . SITEPRF . '-' . TESTS_APP_ENV . ' < ' . $filename;

      $output = array();
      $return_value = 0;
//       echo
      $cmd = 'mysql'
          . ' -h ' . $databases['default']['default']['host']
          . ' -u ' . $databases['default']['default']['username']
          . ' -p' . $databases['default']['default']['password']
          . ' ' . $databases['default']['default']['database']
          . ' < ' . $filename;
      exec($cmd, $output, $return_value);

      if($return_value !== 0) {
        throw new Exception(t('Error return status from !cmd', array('!cmd' => $cmd)));
      }


    }
  }

  /**
   * Search in $haystack and get the reference of the element pointed by parents.
   * Create one if not found. Works on array, stdClass
   *
   * @param mixed $haystack haystack
   * @param array $parents parents of the element to search for (or create it)
   * @param mixed $default_value the value for created (not found) element reference
   * @param mixed $stdClass the type of the intermediate not found levels that will be created (not fount): stdClass or array. default is FALSE
   * @param boolean $set if TRUE, the found reference will be set by $default_value. default FALSE
   * */
  public static function &element_ref_by_parents(&$haystack, $parents, $default_value = array(), $stdClass = FALSE, $set = FALSE) {

      foreach($parents as $i=>$level) {
        // First loop
        if(!$i) {
          $haystack_ref = &$haystack;
        }

        $haystack_ref_tmp = &self::method_prop_key_value($haystack_ref, $level);

        // Create element if doesn't exist
        if($haystack_ref_tmp === NULL) {

          // $default_value for last loop
          $new_element = $i == count($parents)-1 ? $default_value : ($stdClass? new stdClass() : array());

          // TODO so that we could use setter functions
          // ->set{$level}($new_element) or just ->$level($new_element) or some other type of setter function
          if(is_array($haystack_ref)) {
            $haystack_ref[$level] = $new_element;
          }
          else {
            $haystack_ref->$level = $new_element;
          }

          // Replace $haystack reference by reference to current item in $parents with the created element
          $haystack_ref = &self::method_prop_key_value($haystack_ref, $level);
        }
        else {
          // Replace $haystack reference by reference to current item in $parents with the found element
          $haystack_ref = &$haystack_ref_tmp;

          if($i == count($parents)-1 && $set) {
            $haystack_ref = $default_value;
          }
        }
      }

      return $haystack_ref;
    }

    public static function &method_prop_key_value(&$base, $prop) {
      if(is_array($base) && array_key_exists($prop, $base)) {
        $base = &$base[$prop];
      }
      else if(is_callable(array($base, $prop))) {
        $base = &$base->$prop();
      }
      else if(isset($base->$prop)){
        $base = &$base->$prop;
      }
      else {
        $null = NULL;
        return $null;
      }

      return $base;
    }

  /**
   * Reccursively walk on iterable
   *
   * @param mixed $input Must be iterable
   * @param callback $precallback Callback before reccurse going down. signature: $precallback($item, $key, &$context);
   * @param callback $postcallback Callback after reccurse going up. signature: $postcallback($item, $key, &$context);
   * @param callback $leafcallback Callback on leaf element.
   * The return value of this callback determine the reccurse stop condition i.e. the leaf condition.
   *  signature: $leafcallback($item, $key, &$context);
   * @param array $context Available in all callbacks
   * @throws Exception
   */
  public static function element_walk_recursive($input, $precallback, $postcallback, $leafcallback, &$context) {

    foreach($input as $key => $item) {

      if(isset($context['current_path_arr']) && count($context['current_path_arr'])) {
        $current_path = implode('/', $context['current_path_arr']) . '/' . $key;
      }else {
        $current_path = $key;
      }
      $context[$current_path] = $item;
      $context['current_path'] = $current_path;

      if(!$leafcallback($item, $key, $context)) {

        if(!isset($context['current_path_arr'])) {
          $context['current_path_arr'] = array();
        }
        $context['current_path_arr'][] = $key;
        $precallback($item, $key, $context);

        self::element_walk_recursive($item, $precallback, $postcallback, $leafcallback, $context);

        $poped = array_pop($context['current_path_arr']);
        if($poped != $key) {
          throw new Exception('Key: ' . $key . '; item:' . print_r($item, TRUE) . '; poped:' . $poped . 'current_path: ' . print_r($context['current_path_arr'], true));
        }
        $postcallback($item, $key, $context);

      }

    }
  }

  /**
   *
   *
   * @param String $varname output variable name
   * @param Mixed $var input variable
   * @param Array $structure Paths (level1/level2) to values for output
   * @param unknown $using_path
   * @param string $mode debug|json
   * @return boolean
   *
   * @example<pre>
   *  punit_r(
   *    'page_manager_export_task_handler_load',
   *    page_manager_export_task_handler_load($name),
   *    array(
   *      'conf/did' => NULL,
   *      'name' => NULL
   *  ));
      </pre>
   */
  public static function punit_r($varname, $var, $structure = array()/*, $using_paths = false*/) {

  //   if($using_paths) {
  //     $new_structure = array();
  //     foreach ($structure as $path => $value) {
  //       self::element_ref_by_parents($new_structure, explode('/', $path), $value);
  //     }

  //     $structure = $new_structure;
  //   }

    $context = $output = array();

    if($structure == array()) {
      $output = $var;
    }

  // print_r($structure);
    self::element_walk_recursive($structure,
    function($item, $key, &$context){},
    function($item, $key, &$context){},

    function($item, $key, &$context) use ($var, &$output){

      $count_result = 0;
      if(is_array($item) || is_object($item)) {
        foreach($item as $none) {
          $count_result++;
        }
      }

      // count 0 of iterable
      if($leaf_stop_cond = !$count_result) {


        $current_path = SimpleTestUtilityHelper::context_current_path_fixe($context, $key, $item);

  //                 echo "\nleaf - key: " . $key . '; item: ' . $item . '; path: ' . $current_path;
        $cloned_var = is_object($var) ? clone $var : $var;
        $element = SimpleTestUtilityHelper::element_ref_by_parents($cloned_var, explode('/', $current_path), $item);

        SimpleTestUtilityHelper::element_ref_by_parents($output, explode('/', $current_path), $element, FALSE, TRUE);

      }
      //         print_r(array($actual, $actual_dest));
      return $leaf_stop_cond;
    }, $context);

    if(function_exists('variable_get')) {
      $debug_mode = variable_get('pane_reuse_debug');
    }
    else {
      $debug_mode = DEBUG_MODE;
    }

    if ($debug_mode == 'json') {
      echo "\n" .'{"' . $varname . '" : ' . json_encode($output) . '},';
    }
    elseif ($debug_mode == 'verbose') {
      echo "\n" . $varname .' : ' . var_export($output, true);
    }
    else {
      return array($varname => $output);
    }
  }

  public static function context_current_path_fixe(&$context, $key, $item) {
    if(isset($context['current_path_arr']) && count($context['current_path_arr'])) {
      $current_path = implode('/', $context['current_path_arr']) . '/' . $key;
    }else {
      $current_path = $key;
    }
    $context[$current_path] = $item;

    return $current_path;
  }

  /**
   *
   * @param String $configPhpCode
   * @return String Config in xml format
   */
  public static function configPhp2Xml($configPhpCode, $root_element = 'root', $config_varname = 'config') {

    $config = preg_replace('/array \(\s+\),/', '\'array()\',', $configPhpCode);
    // false
    $config = preg_replace('/false,/', '\'false\',', $config);
    // start
    $config = preg_replace('/array \(/', 'ConfigSimpleXml::__set_state(array(', $config);
    // end
    $config = preg_replace('/(\s+)(\),)(\R)/', '\\1)\\2\\3', $config);
    // in drupal context : replace stdClass, panels_display, ...
    $config = preg_replace('/(stdClass|panels_display)::__set_state/', 'ConfigSimpleXml::__set_state', $config);
    //   echo "\n\$config: " . $config;
    //   $eval_str = '$config = ' . $config . ');';

    $eval_str = $config;
    $eval_str = preg_replace('/^<\?php/m', '', $eval_str);
    $eval_str = preg_replace('/\);$/m', '));', $eval_str);
    eval ($eval_str);

    $configXml = '<' . $root_element . '>' . ${$config_varname} . '</' . $root_element . '>';

    return $configXml;
  }

  /**
   * copy-paste from assertFieldByXPath for returning field simplexml element or qp object
   * */
  public static function fieldByXPath(SimpleTestUtilityTestCase $self, $xpath, $value = NULL, $message = '', $group = 'Other') {
    $fields = $self->xpath($xpath);

    // If value specified then check array for match.
    $found = TRUE;
    if (isset($value)) {
      $found = FALSE;
      if ($fields) {
        foreach ($fields as $field) {
          if (isset($field['value']) && $field['value'] == $value) {
            // Input element with correct value.
            $found = TRUE;
          }
          elseif (isset($field->option)) {
            // Select element found.
            if ($self->getSelectedItem($field) == $value) {
              $found = TRUE;
            }
            else {
              // No item selected so use first item.
              $items = $self->getAllOptions($field);
              if (!empty($items) && $items[0]['value'] == $value) {
                $found = TRUE;
              }
            }
          }
          elseif ((string) $field == $value) {
            // Text area with correct text.
            $found = TRUE;
          }
        }
      }
    }
    //    return $self->assertTrue($fields && $found, $message, $group);
    if($fields) {
      if(function_exists('qp')) {
        $asXML = '';
        foreach($fields as $field) {
          $asXML .= $field->asXML();
        }
        $html_xpath = '/html/body';
        $fields = qp($asXML);
        $fields->xpath($html_xpath . '/*');
      }
    }
    else {
      $self->fail($message ? $message : t('found field'), $group);
    }
    return $fields;
  }

  /**
   * Get all named output into the same array variable
   * ex.
   * callback : drush_lp_ep_deleteaccounts_i2a_batch
   * callback : another_callback
   * will produce callback : array(0 =>drush_lp_ep_deleteaccounts_i2a_batch, another_callback)
   * */
  public static function getJsonValue($json, $var_key) {
    $jsonValue = array();
    foreach($json as $a_json) {
      if(isset($a_json[$var_key])) {
        $jsonValue[] = $a_json[$var_key];
      }
    }

    return $jsonValue;
  }

  /**
   * Get all named output into the same array variable.
   * Every output is keyed
   * Ex.
   * data_save_users : Array
   (
   [16] => Array(...)
   )
   * data_save_users : Array
   (
   [6] => Array(...)
   )
   * will produc
   * data_save_users : Array
   (
   [16] => Array(...),
   [6] => Array(...),
   )
   * */
  public static function getJsonKeyValue($json, $var_key) {
    $jsonValue = array();
    foreach($json as $a_json) {
      if(isset($a_json[$var_key])) {
        $each = each($a_json[$var_key]);
        $jsonValue[$each['key']] = $each['value'];
      }
    }

    return $jsonValue;
  }

  public static function hrefQueryArrToOptions($query){
    $options = '';
    foreach($query as $name => $value){
      $options .= ' --'.$name.'='.$value;
    }

    return $options;
  }

  /**
   *
   * $forms_id = array('event-node-form')
   */
  public static function fixXmlErrors(SimpleTestUtilityTestCase $self, $forms_id) {
    $self->content = preg_replace('/(<\/?)figure|section|header|hgroup|nav|footer(\s|>)/', '\\1div\\2', $self->content);
    foreach($forms_id as $form_id) {
      foreach(array('--2', '--3'/*, ''*/) as $suffix) {
        $self->content = preg_replace('/(id="' . $form_id . $suffix .')"/', '\\1--tmp"', $self->content, 1);
        $self->content = str_replace('id="' . $form_id . $suffix .'"', '', $self->content);
        $self->content = str_replace('id="' . $form_id . $suffix .'--tmp"', 'id="' . $form_id . $suffix .'"', $self->content);
      }
    }
  }

  public static function setCheckboxRadioByXpath(SimpleTestUtilityTestCase $self, $xpath, $check = true) {
    // Fixe xml errors
//     $self->fixXmlErrors($self);
    $self->elements = FALSE;

    $qp = $self->fieldByXPath('/*');
    //$self->fieldByXPath('//input[@name="field_is_director[und]"][@type="checkbox"]');

    // Alter form field_is_director[und] = 0
    $qp1 = $qp->branch()->xpath($xpath);

    // return;
    foreach($qp1->get() as $dom) {
      if($check) {
        $dom->setAttribute('checked', 'checked');
      }
      else {
        $dom->removeAttribute('checked');
      }

      $self->content = $dom->ownerDocument->saveHTML();
      $self->elements = FALSE;
    }

    return $qp1->length;
  }

  public static function uncheckedByXpath(SimpleTestUtilityTestCase $self, $xpath) {
    $qp = $self->fieldByXPath($xpath);

    foreach($qp->get() as $dom) {}
    $self->assertTrue(!empty($dom));
    if(empty($dom)) return;
    $self->assertEqual($qp->length, 1);

    return !$dom->hasAttribute('checked');
  }

  // require php5.4
  function filter_ends_with() {
    return /*$filter_ends_with_fn = */function(/*$self, */$ends_with) {
      $self = $this;

      $self->branch()->xpath('self::*[substring(name(.), string-length(name(.))-string-length("' . $ends_with . '")+1) = "' . $ends_with . '"]');
    };
  }

  /**
   *
   * @param Querypath $self
   * @param String $ends_with
   */
  public static function filter_ends_with_fn(SimpleTestUtilityTestCase $self, $ends_with) {
    return $self->branch()->xpath('self::*[substring(name(.), string-length(name(.))-string-length("' . $ends_with . '")+1) = "' . $ends_with . '"]');
  }

  /**
   * drush dl code_coverage
   * testDbSelectEntityLoadEFQ executes on testing database
   */
  public static function testDbSelectEntityLoadEFQ(SimpleTestUtilityTestCase $self) {

    $rel_path = 'documents';


    ////////////////// Starting is empty
    $entity0 = entity_load_single('webdav_stream_entity', 'dav://svn2s2i/owncloud/remote.php/webdav/' . $rel_path);
    $entity0->field_stream_uri_uniquness = array();
    entity_save('webdav_stream_entity', $entity0);

    //////////////////// db_select
    $query = db_select('field_data_field_stream_uri_uniquness', 'field_stream_uri_uniquness')
    ->fields('field_stream_uri_uniquness')
    ->condition('entity_id', crc32('dav://svn2s2i/owncloud/remote.php/webdav/' . $rel_path));
    $self->assertEqual($query->execute()->fetchAll(), array());

    ///////////////// EFQ
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'webdav_stream_entity')
    ->fieldCondition('field_stream_uri_uniquness', 'value', 'dav://svn2s2i/owncloud/remote.php/webdav/' . $rel_path, '=')
    //     ->addMetaData('account', user_load(1))
    ; // Run the query as user 1.
    $result = $query->execute();
    $self->assertEqual($result, array());

    //////////////////// entity_load
    $entity0 = entity_load_single('webdav_stream_entity', 'dav://svn2s2i/owncloud/remote.php/webdav/' . $rel_path);

    $self->assertEqual(array(
        'uri' => $entity0->uri,
        'dav_resourcetype' => $entity0->dav_resourcetype,
        'field_stream_uri_uniquness' => is_array($entity0->field_stream_uri_uniquness) && count($entity0->field_stream_uri_uniquness)
    ),
        array(
            'uri' => 'dav://svn2s2i/owncloud/remote.php/webdav/' .$rel_path,
            'dav_resourcetype' => 'dav_collection',
            'field_stream_uri_uniquness' => 0
        ));



    // ids
    $self->drupalGet('admin/content/webdav/edit/' . $rel_path);

    $self->drupalPost(NULL, array(
        'field_stream_uri_uniquness[und][0][value]' => 'some text will be overriden by uri',
        'field_collection_text[und][0][value]' => 'some text',
    ), t('Save'));

    $self->assertFieldByName('field_stream_uri_uniquness[und][0][value]', 'dav://svn2s2i/owncloud/remote.php/webdav/' . $rel_path);
    $self->assertFieldByName('field_collection_text[und][0][value]', 'some text');


    // parent_uri, ids false
    $self->drupalGet('admin/content/webdav/list/' . $rel_path);
//     $self->assertRaw('<ul class="webdav_stream_entity_list"><li class="webdav_stream_entity_list_item first">1730995982=>dav://svn2s2i/owncloud/remote.php/webdav/documents/Inside%20not%20shared/</li>
// <li class="webdav_stream_entity_list_item last">651141015=>dav://svn2s2i/owncloud/remote.php/webdav/documents/example.odt</li>
// </ul>');
    // @todo assert 2 entries: Can exists a third entry
    $self->assertRaw('1730995982=>dav://svn2s2i/owncloud/remote.php/webdav/documents/Inside%20not%20shared/');
    $self->assertRaw('651141015=>dav://svn2s2i/owncloud/remote.php/webdav/documents/example.odt');


    ///////////////// With field table entry
    ///////////////// with cache
    $entity = entity_load_single('webdav_stream_entity', 'dav://svn2s2i/owncloud/remote.php/webdav/' . $rel_path);

    $self->assertEqual(array(
        'uri' => $entity->uri,
        'dav_resourcetype' => $entity->dav_resourcetype,
        'field_stream_uri_uniquness' => is_array($entity->field_stream_uri_uniquness) && count($entity->field_stream_uri_uniquness)
    ),
        array(
            'uri' => 'dav://svn2s2i/owncloud/remote.php/webdav/' .$rel_path,
            'dav_resourcetype' => 'dav_collection',
            'field_stream_uri_uniquness' => 0
        ));

    /////////////////// no cache
    $entities = entity_load('webdav_stream_entity', array('dav://svn2s2i/owncloud/remote.php/webdav/' . $rel_path), array(), TRUE);
    $entity = reset($entities);

    $self->assertEqual(array(
        'uri' => $entity->uri,
        'dav_resourcetype' => $entity->dav_resourcetype,
        'field_stream_uri_uniquness' => is_array($entity->field_stream_uri_uniquness) && count($entity->field_stream_uri_uniquness)
    ),
        array(
            'uri' => 'dav://svn2s2i/owncloud/remote.php/webdav/' .$rel_path,
            'dav_resourcetype' => 'dav_collection',
            'field_stream_uri_uniquness' => 1
        ));


    /////////////// Without field table entry
    $entity1 = entity_load_single('webdav_stream_entity', 'dav://svn2s2i/owncloud/remote.php/webdav/documents/example.odt');

    $self->assertEqual(array(
        'uri' => $entity1->uri,
        'dav_resourcetype' => $entity1->dav_resourcetype,
        'field_stream_uri_uniquness' => is_array($entity1->field_stream_uri_uniquness) && count($entity1->field_stream_uri_uniquness)
    ),
        array(
            'uri' => 'dav://svn2s2i/owncloud/remote.php/webdav/documents/example.odt',
            'dav_resourcetype' => 'dav_nocollection',
            'field_stream_uri_uniquness' => 0
        ));

    ////////////////// Db select
    $query = db_select('field_data_field_stream_uri_uniquness', 'stream_uri_uniquness')
    ->fields('stream_uri_uniquness')
    ->condition('entity_id', crc32('dav://svn2s2i/owncloud/remote.php/webdav/documents'));

    $self->assertEqual($query->execute()->fetchAll(), array((object)array(
        'entity_type' => 'webdav_stream_entity',
        'bundle' => 'dav_collection',
        'deleted' => '0',
        'entity_id' => '835874057',
        'revision_id' => '835874057',
        'language' => 'und',
        'delta' => '0',
        'field_stream_uri_uniquness_value' => 'dav://svn2s2i/owncloud/remote.php/webdav/documents',
        'field_stream_uri_uniquness_format' => NULL,
    )));

    ///////////////// EFQ
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'webdav_stream_entity')
    ->fieldCondition('field_stream_uri_uniquness', 'value', 'dav://svn2s2i/owncloud/remote.php/webdav/' . $rel_path, '=')
    //     ->addMetaData('account', user_load(1))
    ; // Run the query as user 1.
    $result = $query->execute();
    $self->assertEqual($result, array(
        'webdav_stream_entity' => array(
            835874057 => (object)(array(
                'id' => '835874057',
                'dav_resourcetype' => 'dav_collection',
            )),
        ),
    ));
  }
}
/**
 *
 * eval('$config = ' . var_export(simplexml_load_string($xml, 'SimpleXmlConfig'), true) . ';');
//   var_export($config);


  // TODO ^H
  //   array \( --> ConfigSimpleXml::__set_state(array(
  //   \s+\),\R -->)),
  eval('$config = ' . var_export(simplexml_load_string($xml, 'ConfigSimpleXml'), true) . ';');
 *
 */
class SimpleXmlConfig extends SimpleXMLElement{
  public function __set_state($array) {
    $output = count($array) ? array() : '';
    //     print_r($array);
    foreach ($array as $key => $value) {
      $key = str_replace('___', ' ', $key);
      if($value == 'array()') {
        $value = array();
      }
      else if($value == 'false') {
        $value = false;
      }

      // Array with numeric indexes.
      $matches = array();
      if(preg_match('/^_([0-9]+)$/', $key, $matches)) {
        $output[$matches[1]] = $value;
      }
      else {
        $output[$key] = $value;
      }
    }

    return $output;
  }
}

class ConfigSimpleXml extends SimpleXMLElement{
  public function __set_state($array) {
    $output = '';
    if(count($array)) {
      foreach ($array as $key => $value) {
        $key = str_replace(' ', '___', $key);

        // Array with numeric indexes.
        if(is_numeric($key)) {
          $key = '_' . $key;
        }

        // Prefix, Suffix can have open html tags.
        // body for panes. other possible body key?
        if(in_array($key, array('prefix', 'suffix', 'body'))) {
          $value = '<![CDATA[' . $value . ']]>';
        }

        $output .= '<' . $key . '>' . $value . '</' . $key . '>' . "\n";
      }
    }
    else {

    }

    return $output;
  }
}