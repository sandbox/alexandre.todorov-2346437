<?php
/**
 * @file
 * Drush command for deleteaccounts module
 */


/**
 * Callback function for webservices
 * Dispatch params
 *
 * */
function simpletest_utility_fn_call() {

  $ws_name = func_get_args();

  switch ($ws_name[0]) {

    default:
      $callparams = $ws_name;
      $method = array_shift($callparams);
      call_user_func_array($method, $callparams);

      break;

  }
}

/**
 * Implements hook_drush_command().
 * drush simpletest_clone_map_webform_nid
 */
function simpletest_utility_drush_command() {
  $items = array();

  $items['fn-call'] = array(
    'callback' => 'simpletest_utility_fn_call',
    'options' => array(
      'opt1' => 'Opt 1 desc',
//       'opt2' => 'Opt 2 desc',
    ),
  );
  $items['test-fe'] = array(
    'callback' => 'simpletest_utility_test_fe',
  );

  return $items;
}

/**
 * drush test-fe [--features] [--cmpnts]
 * To boost performance, first time call only "drush test-fe" for having all elements is {feature}.xml files.
 * Then for subset call "drush test-fe --features={feature1,feature2} --cmpnts={cmpnt1, cmpnt2}"
 */
function simpletest_utility_test_fe() {
  $cmpnts = drush_get_option('cmpnts')? explode(',', drush_get_option('cmpnts')) : array();
  $features = drush_get_option('features')? explode(',', drush_get_option('features')) : array();

  list($return, $db, $diff) = simpletest_utility_config_all($features, $cmpnts);

  echo var_export($return);
  echo var_export($db);
  echo var_export($diff);
}
