<?php

/**
 * @file
 * Helper test case for cloning an existing Drupal configuration.
 */

/**
 * Wrapper for parent protected methods
 */
interface SimpleTestUtilityTestCase {

  public function getSelectedItem(SimpleXMLElement $element);

  public function xpath($xpath, array $arguments = array());

  public function getAllOptions(SimpleXMLElement $element);

  public function fail($message = NULL, $group = 'Other');

  public function assertTrue($value, $message = '', $group = 'Other');

  public function assertEqual($first, $second, $message = '', $group = 'Other');

  public function assertRaw($raw, $message = '', $group = 'Other');

  public function assertFieldByName($name, $value = '', $message = '');

  public function drupalGet($path, array $options = array(), array $headers = array());

  public function drupalPost($path, $edit, $submit, array $options = array(), array $headers = array(), $form_html_id = NULL, $extra_post = NULL);


  public function getContent();

  public function setContent($content);

  public function getElements();

  public function setElements($elements);

  public function getOriginalFileDirectory();

  public function setOriginalFileDirectory($originalFileDirectory);

  public function getDatabasePrefix();

  public function setDatabasePrefix($databasePrefix);
//   public function parse();
}


/**
 * Clone existing Drupal instance.
 */
class SimpleTestMysqldumpTestCase extends SimpleTestCloneTestCase implements SimpleTestUtilityTestCase {

  /**
   * values :
   *   FALSE - call parent::cloneTable(),
   *   TRUE - on SetUp(),
   *   drupal_static (@todo page request: on dev mode to speed up)
   *     attention to $this->databasePrefix - see parent::setUp()
   * @var unknown
   */
  protected $mysqldump = TRUE;

  /**
   * Wrapper for parent protected attributes
   */
  public function getContent() {
    return $this->content;
  }

  public function setContent($content) {
    $this->content = $content;
  }

  public function getElements() {
    return $this->elements;
  }

  public function setElements($elements) {
    $this->elements = $elements;
  }

  public function getOriginalFileDirectory() {
    return $this->originalFileDirectory;
  }

  public function setOriginalFileDirectory($originalFileDirectory) {
    $this->originalFileDirectory = $originalFileDirectory;
  }

  public function getDatabasePrefix() {
    return $this->databasePrefix;
  }

  public function setDatabasePrefix($databasePrefix) {
    $this->databasePrefix = $databasePrefix;
  }

  /**
   * Mirror over an existing tables structure, and copy the data.
   *
   * @param $name
   *   Table name.
   * @param $schema
   *   A Schema API definition array.
   * @return
   *   Array of table creation results.
   */
  protected function cloneTable($name, $source, $schema) {
    global $databases;

    if(!$this->mysqldump) {
      return parent::cloneTable($name, $source, $schema);
    }

    require_once 'simpletest_utility.inc';
    SimpleTestUtilityHelper::dumpPregReplace($this);
  }

  public function testWithHelper() {
    // @todo drop next line
    require_once 'simpletest_utility.inc';

    SimpleTestUtilityHelper::testDbSelectEntityLoadEFQ($this);
  }

  /**
   * Wrapper for parent protected methods
   */
  public function getSelectedItem(SimpleXMLElement $element) {
    return parent::getSelectedItem($element);
  }

  public function xpath($xpath, array $arguments = array()) {
    return parent::xpath($xpath, $arguments);
  }

  public function getAllOptions(SimpleXMLElement $element) {
    return parent::getAllOptions($element);
  }

  public function fail($message = NULL, $group = 'Other') {
    return parent::fail($message, $group);
  }

  public function assertTrue($value, $message = '', $group = 'Other') {
    return parent::assertTrue($value, $message, $group);
  }

  public function assertEqual($first, $second, $message = '', $group = 'Other') {
    return parent::assertEqual($first, $second, $message, $group);
  }

  public function assertRaw($raw, $message = '', $group = 'Other') {
    return parent::assertRaw($raw, $message, $group);
  }

  public function assertFieldByName($name, $value = '', $message = '') {
    return parent::assertFieldByName($name, $value, $message);
  }

  public function drupalGet($path, array $options = array(), array $headers = array()) {
    return parent::drupalGet($path, $options, $headers);
  }

  public function drupalPost($path, $edit, $submit, array $options = array(), array $headers = array(), $form_html_id = NULL, $extra_post = NULL) {
    return parent::drupalPost($path, $edit, $submit, $options, $headers, $form_html_id, $extra_post);
  }
}

class SimpleTestWebTestCase extends DrupalWebTestCase  implements SimpleTestUtilityTestCase{

  public static function getInfo() {
    return array(
        'name' => 'SimpleTest Web Test',
        'description' => 'Ensure that the simpletest_example content type provided functions properly.',
        'group' => 'Examples',
    );
  }

  public function testWithHelper() {
    // @todo drop next line
    require_once 'simpletest_utility.inc';

    SimpleTestUtilityHelper::testDbSelectEntityLoadEFQ($this);
  }

  public function prepareEnvironment() {
    return parent::prepareEnvironment();
  }

  /**
   * Wrapper for parent protected methods
   */
  public function getSelectedItem(SimpleXMLElement $element) {
    return parent::getSelectedItem($element);
  }

  public function xpath($xpath, array $arguments = array()) {
    return parent::xpath($xpath, $arguments);
  }

  public function getAllOptions(SimpleXMLElement $element) {
    return parent::getAllOptions($element);
  }

  public function fail($message = NULL, $group = 'Other') {
    return parent::fail($message, $group);
  }

  public function assertTrue($value, $message = '', $group = 'Other') {
    return parent::assertTrue($value, $message, $group);
  }

  public function assertEqual($first, $second, $message = '', $group = 'Other') {
    return parent::assertEqual($first, $second, $message, $group);
  }

  public function assertRaw($raw, $message = '', $group = 'Other') {
    return parent::assertRaw($raw, $message, $group);
  }

  public function assertFieldByName($name, $value = '', $message = '') {
    return parent::assertFieldByName($name, $value, $message);
  }

  public function drupalGet($path, array $options = array(), array $headers = array()) {
    return parent::drupalGet($path, $options, $headers);
  }

  public function drupalPost($path, $edit, $submit, array $options = array(), array $headers = array(), $form_html_id = NULL, $extra_post = NULL) {
    return parent::drupalPost($path, $edit, $submit, $options, $headers, $form_html_id, $extra_post);
  }

  /**
   * Wrapper for parent protected attributes
   */
  public function getContent() {
    return $this->content;
  }

  public function setContent($content) {
    $this->content = $content;
  }

  public function getElements() {
    return $this->elements;
  }

  public function setElements($elements) {
    $this->elements = $elements;
  }

  public function getOriginalFileDirectory() {
    return $this->originalFileDirectory;
  }

  public function setOriginalFileDirectory($originalFileDirectory) {
    $this->originalFileDirectory = $originalFileDirectory;
  }

  public function getDatabasePrefix() {
    return $this->databasePrefix;
  }

  public function setDatabasePrefix($databasePrefix) {
    $this->databasePrefix = $databasePrefix;
  }

  public function drupalLogin(stdClass $account) {
    return parent::drupalLogin($account);
  }

  public function getOriginalUser() {
    return $this->originalUser;
  }

  public function setOriginalUser($account) {
    $this->originalUser = $account;
  }
}

/**
 * Uses SimpleTestWebTestCase to perform web specific request (drupalGet(), ...).No auto clean of prefixed tables and files
 * For speed up don't clean environement to keep mysqldumped database tables and files.
 * Usefull for big databases when dump-insert takes a time on every setUp()
 * When finished clean environement before execute other tests
 *
 * Re run doesn't work because want to execute also $this->webTestCase Test Case
 */
class SimpleTestUnitWebTestCase extends DrupalUnitTestCase implements SimpleTestUtilityTestCase{

  protected $webTestCase = null;

//   public $originalFileDirectory;
//   public $databasePrefix;

  public function setUp() {

    $this->webTestCase = new SimpleTestWebTestCase($this->testId);
//     $this->testId = $test_id;

    foreach (array('devel', 'views', 'entity', 'picasa', 'remote_stream_wrapper', 'webdav_stream_wrapper', 'webdav_stream_entity') as $module) {
      //       drupal_load('module', 'simpletest_example');
      drupal_load('module', $module);
    }


    // Start parent::setUp();
    code_coverage_stop(FALSE);

    global $conf;

    // Store necessary current values before switching to the test environment.
    $this->originalFileDirectory = variable_get('file_public_path', conf_path() . '/files');

    // Reset all statics so that test is performed with a clean environment.
    drupal_static_reset();

    // Generate temporary prefixed database to ensure that tests have a clean starting point.
    $this->databasePrefix = Database::getConnection()->prefixTables('{simpletest' . '666' . /*mt_rand(1000, 1000000) .*/ '}');

    // Create test directory.
    $public_files_directory = $this->originalFileDirectory . '/simpletest/' . substr($this->databasePrefix, 10);
    file_prepare_directory($public_files_directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    $conf['file_public_path'] = $public_files_directory;

    // Clone the current connection and replace the current prefix.
    $connection_info = Database::getConnectionInfo('default');
    Database::renameConnection('default', 'simpletest_original_default');
    foreach ($connection_info as $target => $value) {
      $connection_info[$target]['prefix'] = array(
          'default' => $value['prefix']['default'] . $this->databasePrefix,
      );
    }
    Database::addConnectionInfo('default', 'default', $connection_info['default']);

    // Set user agent to be consistent with web test case.
    $_SERVER['HTTP_USER_AGENT'] = $this->databasePrefix;

    // If locale is enabled then t() will try to access the database and
    // subsequently will fail as the database is not accessible.
    $module_list = module_list();
    if (isset($module_list['locale'])) {
      // Transform the list into the format expected as input to module_list().
      foreach ($module_list as &$module) {
        $module = array('filename' => drupal_get_filename('module', $module));
      }
      $this->originalModuleList = $module_list;
      unset($module_list['locale']);
      module_list(TRUE, FALSE, FALSE, $module_list);
    }
    $this->setup = TRUE;

    code_coverage_start();

    // End parent::setUp()

    $this->webTestCase->setDatabasePrefix($this->databasePrefix);
    $this->prepareEnvironment();

    require_once 'simpletest_utility.inc';
    SimpleTestUtilityHelper::dumpPregReplace($this);

  }

  /* Delete this line for testDbSelectEntityLoadEFQ on same database*/
  public function testWithHelper() {
    // @todo drop next line
    require_once 'simpletest_utility.inc';

    SimpleTestUtilityHelper::testDbSelectEntityLoadEFQ($this);
  }// */

  public function fail($message = NULL, $group = 'Other') {
    return parent::fail($message, $group);
  }

  public function assertTrue($value, $message = '', $group = 'Other') {
    return parent::assertTrue($value, $message, $group);
  }

  public function assertEqual($first, $second, $message = '', $group = 'Other') {
    return parent::assertEqual($first, $second, $message, $group);
  }

  public function assertRaw($raw, $message = '', $group = 'Other') {
    return $this->webTestCase->assertRaw($raw, $message, $group);
  }

  public function getSelectedItem(SimpleXMLElement $element) {
    return $this->webTestCase->getSelectedItem($element);
  }

  public function prepareEnvironment() {
    return $this->webTestCase->prepareEnvironment();
  }

  public function xpath($xpath, array $arguments = array()) {
    return $this->webTestCase->xpath($xpath, $arguments);
  }

  public function getAllOptions(SimpleXMLElement $element) {
    return $this->webTestCase->getAllOptions($element);
  }

  public function assertFieldByName($name, $value = '', $message = '') {
    return $this->webTestCase->assertFieldByName($name, $value, $message);
  }

  public function drupalGet($path, array $options = array(), array $headers = array()) {
    return $this->webTestCase->drupalGet($path, $options, $headers);
  }

  public function drupalPost($path, $edit, $submit, array $options = array(), array $headers = array(), $form_html_id = NULL, $extra_post = NULL) {
    return $this->webTestCase->drupalPost($path, $edit, $submit, $options, $headers, $form_html_id, $extra_post);
  }

  /**
   * Wrapper for parent protected attributes
   */
  public function getContent() {
    return $this->webTestCase->getContent();
  }

  public function setContent($content) {
    $this->webTestCase->setContent($content);
  }

  public function getElements() {
    return $this->webTestCase->getElements();
  }

  public function setElements($elements) {
    $this->webTestCase->setElements($elements);
  }

  public function getOriginalFileDirectory() {
    return $this->webTestCase->getOriginalFileDirectory();
  }

  public function setOriginalFileDirectory($originalFileDirectory) {
    $this->webTestCase->setOriginalFileDirectory($originalFileDirectory);
  }

  public function getDatabasePrefix() {
    return $this->webTestCase->getDatabasePrefix();
  }

  public function setDatabasePrefix($databasePrefix) {
    $this->webTestCase->setDatabasePrefix($databasePrefix);
  }

  public function drupalLogin(stdClass $account) {
    $this->webTestCase->drupalLogin($account);
  }

  public function getOriginalUser() {
    return $this->webTestCase->getOriginalUser();
  }

  public function setOriginalUser($account) {
    $this->webTestCase->setOriginalUser($account);
  }

}


